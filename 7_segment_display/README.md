# 7セグメントディスプレイ LED 
セグメントディスプレイ LED チューブモジュール 0.36 インチ TM1637


<img src="../images/71kp8hiMsPL._AC_SL1500_.jpg" width=512>

- 4桁の赤の表示
- 制御インターフェイス：合計4ピン（GND、VCC、DIO、CLK）、GND-グランド、VCC-電源、DIO-データ入出力ピン、CLK-クロック信号ピン
- 電圧5Vまたは3.3V
- 4つのM2ねじ位置決め穴
- 0.36インチのLEDディスプレイ●サイズ 47mm×19mm×12mm

TM1637

信号線が２つで４桁を表示できる
##

## 
[TM1637 MicroPython library ](https://github.com/mcauser/micropython-tm1637)



## 参考にしたリンク

[How to Use a 7-Segment Display with Raspberry Pi Pico](https://www.tomshardware.com/how-to/raspberry-pi-pico-7-segment-display)
