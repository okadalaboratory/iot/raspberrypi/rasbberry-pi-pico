# Rasbberry Pi Pico

<img src="images/M-18086.jpg" width=512>

## 仕様
Raspberry Pi Pico W

- RP2040（ARM Cortex-M0+ デュアル コア @ 133MHz）
- WiFi, Bluetooth
- SPI、I2C、UART、PWM
- アナログ/デジタル コンバーター (ADC)
- プログラマブルI/O（PIO）
- Python（MicroPython）、C/C++
- 他のRaspberry Piとは異なりLinux OSは搭載できない
- 1,000円程度

[Raspberry Pi Pico データシート - raspberrypi.com](https://datasheets.raspberrypi.com/pico/pico-datasheet.pdf)
[RP2040  データシート(マイコン) - raspberrypi.com](https://datasheets.raspberrypi.com/rp2040/rp2040-datasheet.pdf)

## ピンレイアウト
<img src="images/4e905a76-ffcd-4322-8bf0-5cbebc4f360c.png" width=512>

## 初期セットアップ
1. BOOTSELモードで起動
PicoのBOOTSELボタンを押しながらUSBケーブルをPCに接続すると、BOOTSELモードで起動し、PCのドライブ(RPI-PR2)としてアクセスすることができるようになります。

2. MicroPythonファームウェア（UF2ファイル）のダウンロード
INDEX.HTMをクリックすることで、Raspberry PiのRP2024ページが開くので、MicroPythonのスタートページに行き、MicroPythonファームウェア（UF2ファイル）をダウンロードします。

ダウンロードしたファームウェアをPicoのRPI-RP2ドライブにコピーします。自動的にPicoが再起動し準備が整いました。

3. 開発環境のインストール
MicroPythonを使っての開発はエストニアのTartu(タルトゥ)大学が開発した“Thonny”(トニーまたはソニーと読む)を使います。

[ThonnyのWebサイト](https://thonny.org/)からお使いのPCに合わせたプログラムをインストールしてください。

<img src="images/screenshot.png" width=512>

4. 確認
Thonnyを起動し、

Thonnyの起動画面．シェルに“>>>”が表示されていたら，開発環境は正常です。


## 最初のプログラム
Pico本体の緑のLEDを使って「Lチカ」プログラムを動かしてましょう。
このLEDはGPIO２５ピンに接続されているので、そのピンへの電圧をH（点灯）にしたりL（消灯）にしたりを繰り返すことで点滅を繰り返すようになります。

```
import machine, utime
led = machine.Pin(25, machine.Pin.OUT)
while True:
  led.value(1)
  utime.sleep(2)
  led.value(0)
  utime.sleep(2)
```

## Lチカの色々

### デューティ比を変えてみる

### PWM（パルス幅制御）

### 1/f揺らぎ


## NeoPixelを使ってみる

## WiFi機能を使ってみる

## Bluetooth機能を使ってみる

## e-Paper
https://www.waveshare.com/e-paper-driver-hat.htm


## リンク
[公式ドキュメント](https://www.raspberrypi.com/documentation/microcontrollers/)

[初めの一歩！ラズパイPicoマイコン×PythonでLチカ入門
開発環境のセットアップから点灯/消灯制御プログラミングまで](https://www.marutsu.co.jp/pc/static/large_order/zep/m-z-picoled-da1)

[Raspberry Pi Picoで最初の一歩、Lチカしてみよう！](https://info.picaca.jp/14899)

[初めの一歩！ラズパイPicoマイコン×PythonでLチカ入門
16連LED“NeoPixel”の点灯/消灯/色/明るさをプログラミングで自由自在](https://www.zep.co.jp/utaguchi/article/z-picoled_all-da1/)

[【Raspberry Pi Pico W】無線LAN機能の使い方完全ガイド](https://sozorablog.com/raspberry-pi-pico-w-review/)<br>
- Pico W本体のLEDをWi-Fi経由で操作する
- 温度センサーの数値をWi-Fi経由で確認する
- BOOTSELボタンを押してLINEにメッセージを送る
- インターネットから情報を取得する
- インターネットから時刻を取得する

[Raspberry Pi Pico W で Httpサーバ(microdot)とセンサーによるHTTPリクエスト機能を同時に稼働させる
](https://tech.torico-corp.com/blog/raspberry-pi-pico-w-microdot-http-server-and-request-async-await/)



[【BLE】RaspberryPi Pico W MicroPythonでBluetoothを使う方法【チュートリアル ペリフェラル編】
](https://tech-and-investment.com/raspberrypi-picow-10-bluetooth1/#lightblue%e3%81%ae%e6%93%8d%e4%bd%9c%e6%96%b9%e6%b3%95)

[New functionality: Bluetooth for Pico W](https://www.raspberrypi.com/news/new-functionality-bluetooth-for-pico-w/)

