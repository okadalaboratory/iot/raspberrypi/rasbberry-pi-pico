# 有機ELディスプレイ

SSD1306

Hailege 0.96" SSD1306 I2C IIC OLED LCDディスプレイ128X64 OLED 51 STM33 Arduino Raspberry Pi用 (ホワイトフォント)

<img src="../images/610fro6wsiL._AC_SL1010___1_.jpg" width=512>


- ドライバーIC：SSD1306
- ドライブデューティ：1/64デューティ
- 電圧：DC3V〜5V
- 高解像度：128 * 64
- 0.96 インチ


##



## 参考にしたリンク
[ラズパイPicoでSSD1306有機ELディスプレイの使い方 MicroPython編](https://logikara.blog/raspi_pico_oled_micropy/#mokuji_5)

