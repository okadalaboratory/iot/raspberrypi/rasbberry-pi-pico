# I2C LCD 1602

<img src="../images/71xFXYb+R+L._AC_SL1500_.jpg" width=512>


[](https://freenove.com/fnk0079)

[RaspberryPiPicoとRaspberryPiでI2Cを使うときに気を付ける ](https://www.hobbyhappyblog.jp/raspberrypipico-raspbrrypi-i2c-diff)

[I2C接続のLCDディスプレイを使う方法](https://hellobreak.net/raspberry-pi-pico-i2c-lcd-0214/)

[Raspberry Pi Pico MicroPython I2C LCD1602表示テスト](https://blog.goo.ne.jp/jh7ubc/e/dece00d26ebf81c38ed5146808cee534)



# セットアップ

# ライブラリ
[ダウンロード](https://freenove.com/fnk0079)

I2C_LCD.py

LCD_API.py

IIC_LCD1602.py
```
import time
from machine import I2C, Pin
from I2C_LCD import I2CLcd

i2c = I2C(0, sda=Pin(8), scl=Pin(9), freq=400000)
devices = i2c.scan()
print(i2c.scan())

try:
    if devices != []:
        lcd = I2CLcd(i2c, devices[0], 2, 16)
        lcd.move_to(0, 0)
        lcd.putstr("Hello, world!")
        count = 0
        while True:
            lcd.move_to(0, 1)
            lcd.putstr("Counter:%d" %(count))
            time.sleep(1)
            count += 1
            
    else:
        print("No address found")
except:
    pass
```