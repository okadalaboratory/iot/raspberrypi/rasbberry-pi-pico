# 2.13インチ 赤黒白 電子ペーパー

[2.13インチ 212X104 赤黒白 電子ペーパー モジュール E-Paper ディスプレイ HAT for RPI Raspberry Pi Pico W WH ラズパイ ピコ ラズベリーパイ ピコ W](https://www.amazon.co.jp/gp/product/B09SHMV8L1/ref=ppx_yo_dt_b_asin_title_o02_s00?ie=UTF8&psc=1)

<img src="../../images/51s1OX2FT3L._AC_SL1000_.jpg" width=512>


## 
[メーカーWiKi](www.waveshare.com/wiki/Pico-ePaper-2.13-B)


## サンプルプログラム
お使いのPCで
```
cd ~
sudo wget  https://files.waveshare.com/upload/2/27/Pico_ePaper_Code.zip
unzip Pico_ePaper_Code.zip -d Pico_ePaper_Code
cd ~/Pico_ePaper_Code/python
```
Pico_ePaper-2.14_V4.py
```
# *****************************************************************************
# * | File        :	  Pico_ePaper-2.13-B_V4.py
# * | Author      :   Waveshare team
# * | Function    :   Electronic paper driver
# * | Info        :
# *----------------
# * | This version:   V1.0
# * | Date        :   2022-08-22
# # | Info        :   python demo
# -----------------------------------------------------------------------------
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documnetation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to  whom the Software is
# furished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS OR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

from machine import Pin, SPI
import framebuf
import utime


EPD_WIDTH       = 122
EPD_HEIGHT      = 250

RST_PIN         = 12
DC_PIN          = 8
CS_PIN          = 9
BUSY_PIN        = 13

class EPD_2in13_B_V4:
    def __init__(self):
        self.reset_pin = Pin(RST_PIN, Pin.OUT)
        
        self.busy_pin = Pin(BUSY_PIN, Pin.IN, Pin.PULL_UP)
        self.cs_pin = Pin(CS_PIN, Pin.OUT)
        if EPD_WIDTH % 8 == 0:
            self.width = EPD_WIDTH
        else :
            self.width = (EPD_WIDTH // 8) * 8 + 8
        self.height = EPD_HEIGHT
        
        self.spi = SPI(1)
        self.spi.init(baudrate=4000_000)
        self.dc_pin = Pin(DC_PIN, Pin.OUT)
        
        
        self.buffer_balck = bytearray(self.height * self.width // 8)
        self.buffer_red = bytearray(self.height * self.width // 8)
        self.imageblack = framebuf.FrameBuffer(self.buffer_balck, self.width, self.height, framebuf.MONO_HLSB)
        self.imagered = framebuf.FrameBuffer(self.buffer_red, self.width, self.height, framebuf.MONO_HLSB)
        self.init()

    def digital_write(self, pin, value):
        pin.value(value)

    def digital_read(self, pin):
        return pin.value()

    def delay_ms(self, delaytime):
        utime.sleep(delaytime / 1000.0)

    def spi_writebyte(self, data):
        self.spi.write(bytearray(data))

    def module_exit(self):
        self.digital_write(self.reset_pin, 0)

    # Hardware reset
    def reset(self):
        self.digital_write(self.reset_pin, 1)
        self.delay_ms(50)
        self.digital_write(self.reset_pin, 0)
        self.delay_ms(2)
        self.digital_write(self.reset_pin, 1)
        self.delay_ms(50)


    def send_command(self, command):
        self.digital_write(self.dc_pin, 0)
        self.digital_write(self.cs_pin, 0)
        self.spi_writebyte([command])
        self.digital_write(self.cs_pin, 1)

    def send_data(self, data):
        self.digital_write(self.dc_pin, 1)
        self.digital_write(self.cs_pin, 0)
        self.spi_writebyte([data])
        self.digital_write(self.cs_pin, 1)
        
    def send_data1(self, buf):
        self.digital_write(self.dc_pin, 1)
        self.digital_write(self.cs_pin, 0)
        self.spi.write(bytearray(buf))
        self.digital_write(self.cs_pin, 1)
        
    def ReadBusy(self):
        print('busy')
        while(self.digital_read(self.busy_pin) == 1): 
            self.delay_ms(10) 
        print('busy release')
        self.delay_ms(20)
        
    def TurnOnDisplay(self):
        self.send_command(0x20)  # Activate Display Update Sequence
        self.ReadBusy()

    def SetWindows(self, Xstart, Ystart, Xend, Yend):
        self.send_command(0x44) # SET_RAM_X_ADDRESS_START_END_POSITION
        self.send_data((Xstart>>3) & 0xFF)
        self.send_data((Xend>>3) & 0xFF)

        self.send_command(0x45) # SET_RAM_Y_ADDRESS_START_END_POSITION
        self.send_data(Ystart & 0xFF)
        self.send_data((Ystart >> 8) & 0xFF)
        self.send_data(Yend & 0xFF)
        self.send_data((Yend >> 8) & 0xFF)
        
    def SetCursor(self, Xstart, Ystart):
        self.send_command(0x4E) # SET_RAM_X_ADDRESS_COUNTER
        self.send_data(Xstart & 0xFF)

        self.send_command(0x4F) # SET_RAM_Y_ADDRESS_COUNTER
        self.send_data(Ystart & 0xFF)
        self.send_data((Ystart >> 8) & 0xFF)
    

    def init(self):
        print('init')
        self.reset()
        
        self.ReadBusy()   
        self.send_command(0x12)  #SWRESET
        self.ReadBusy()   

        self.send_command(0x01) #Driver output control      
        self.send_data(0xf9)
        self.send_data(0x00)
        self.send_data(0x00)

        self.send_command(0x11) #data entry mode       
        self.send_data(0x03)

        self.SetWindows(0, 0, self.width-1, self.height-1)
        self.SetCursor(0, 0)

        self.send_command(0x3C) #BorderWaveform
        self.send_data(0x05)

        self.send_command(0x18) #Read built-in temperature sensor
        self.send_data(0x80)

        self.send_command(0x21) #  Display update control
        self.send_data(0x80)
        self.send_data(0x80)

        self.ReadBusy()
        
        return 0       
        
    def display(self):
        self.send_command(0x24)
        self.send_data1(self.buffer_balck)
        
        self.send_command(0x26)
        self.send_data1(self.buffer_red)  

        self.TurnOnDisplay()

    
    def Clear(self, colorblack, colorred):
        self.send_command(0x24)
        self.send_data1([colorred] * self.height * int(self.width / 8))
        
        self.send_command(0x26)
        self.send_data1([colorred] * self.height * int(self.width / 8))
                                
        self.TurnOnDisplay()

    def sleep(self):
        self.send_command(0x10) 
        self.send_data(0x01)
        
        self.delay_ms(2000)
        self.module_exit()
        
        
if __name__=='__main__':
    epd = EPD_2in13_B_V4()
    epd.Clear(0xff, 0xff)
    
    epd.imageblack.fill(0xff)
    epd.imagered.fill(0xff)
    epd.imageblack.text("Waveshare", 0, 10, 0x00)
    epd.imagered.text("ePaper-2.13B", 0, 25, 0x00)
    epd.imageblack.text("RPi Pico", 0, 40, 0x00)
    epd.imagered.text("Hello World", 0, 55, 0x00)
    epd.display()
    epd.delay_ms(2000)
    
    epd.imagered.vline(10, 90, 40, 0x00)
    epd.imagered.vline(90, 90, 40, 0x00)
    epd.imageblack.hline(10, 90, 80, 0x00)
    epd.imageblack.hline(10, 130, 80, 0x00)
    epd.imagered.line(10, 90, 90, 130, 0x00)
    epd.imageblack.line(90, 90, 10, 130, 0x00)
    epd.display()
    epd.delay_ms(2000)
    
    epd.imageblack.rect(10, 150, 40, 40, 0x00)
    epd.imagered.fill_rect(60, 150, 40, 40, 0x00)
    epd.display()
    epd.delay_ms(2000)
        
    epd.Clear(0xff, 0xff)
    epd.delay_ms(2000)
    print("sleep")
    epd.sleep()
```

##　画像の表示


## カラー（赤白黒）表示



## 日本語の表示
### 参考にしたサイト
[Raspberry Pi Picoと電子ペーパーで日本語を表示する](https://note.com/note50/n/n574fe293ee75)

[開発メモ：電子ペーパー用の日本語表示ライブラリ作成](https://plaza.rakuten.co.jp/washiinuru/diary/202305270000/)

[MicroPython:電子ペーパー用モジュール作成 (2)](https://plaza.rakuten.co.jp/washiinuru/diary/202308240000/)


### フォントテーブルの作成
[東雲フォント](https://ja.wikipedia.org/wiki/%E6%9D%B1%E9%9B%B2%E3%83%95%E3%82%A9%E3%83%B3%E3%83%88)のビットマップデータを使い、フォントテーブルを作成する

[東雲フォントのダウンロード](http://openlab.ring.gr.jp/efont/shinonome/)

1. コード表から、表示したい文字を検索してJISコードを調べる<br>
[半角文字：JIS X 0201 (1976) to Unicode 文字コード表](http://charset.7jp.net/jis0201.html)<br>
[全角文字：JIS X 0208 (1990) to Unicode 漢字コード表](http://charset.7jp.net/jis0208.html)<br>
たとえば、半角文字の ”１” なら31となる。

2. 東雲フォントのshnm8x16r.bdf（半角文字）を開き、STARTCHAR 31の行を見つける。<br>
以下のBITMAPの次の行から、ENDCHARの前の行までの16行分がビットマップデータになる。
```
STARTCHAR 31
ENCODING 49
SWIDTH 480 0
DWIDTH 8 0
BBX 8 16 0 -2
BITMAP
00
10
10
30
50
10
10
10
10
10
10
10
10
10
00
00
ENDCHAR
```
下記をフォントテーブルに追加する。<br>
[0x31,0x00,0x10,0x10,0x30,0x50,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x00,0x00],


全角文字についても同様にする。

漢字の「玉」は 364c(JIS),0xE78E89(UTF)である。
```
STARTCHAR 364c
ENCODING 13900
SWIDTH 960 0
DWIDTH 16 0
BBX 16 16 0 -2
BITMAP
0000
3ffe
0080
0080
0080
0080
0080
1ffc
0080
0080
0090
0088
0088
0080
7fff
0000
ENDCHAR
```
下記をフォントテーブルに追加する。<br>

[0xE78E89,0x0000,0x3ffe,0x0080,0x0080,0x0080,0x0080,0x0080,0x1ffc,0x0080,0x0080,0x0090,0x0088,0x0088,0x0080,0x7fff,0x0000]


[JISコードとUTF-8コードの対応表](https://note.com/api/v2/attachments/download/3ca5a3b12d1ff600120f1ee176b5b249)


font16.py
```
fonta16=(
[0x20,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00], # 20
[0x25,0x01,0x61,0x92,0x92,0x94,0x94,0x68,0x08,0x10,0x16,0x29,0x29,0x49,0x49,0x86,0x80], # 25 %
[0x2c,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x60,0x20,0x20,0x40], # 2c ,
[0x2d, 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xfe, 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00], # 2d -
[0x2e,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x60,0x60,0x00,0x00], # 2e .
[0x2f,0x00,0x02,0x02,0x04,0x04,0x08,0x08,0x10,0x10,0x20,0x20,0x40,0x40,0x80,0x80,0x00], # 2f /
[0x30,0x00,0x18,0x24,0x24,0x42,0x42,0x42,0x42,0x42,0x42,0x42,0x24,0x24,0x18,0x00,0x00], # 30 0
[0x31,0x00,0x10,0x10,0x30,0x50,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x00,0x00], # 31 1
[0x32,0x00,0x18,0x24,0x42,0x42,0x02,0x04,0x08,0x10,0x20,0x20,0x40,0x40,0x7e, 0x00,0x00],# 32 2
[0x33,0x00,0x38,0x44,0x82,0x82,0x02,0x04,0x38,0x04,0x02,0x82,0x82,0x44,0x38,0x00,0x00], # 33 3
[0x34,0x00,0x08,0x18,0x18,0x28,0x28,0x48,0x48,0x88,0xfe, 0x08,0x08,0x08,0x08,0x00,0x00],# 34 4
[0x35,0x00,0x7c,0x40,0x40,0x40,0xb8,0xc4,0x82,0x02,0x02,0x82,0x82,0x44,0x38,0x00,0x00], # 35 5
[0x36,0x00,0x38,0x44,0x40,0x80,0x80,0xb8,0xc4,0x82,0x82,0x82,0x82,0x44,0x38,0x00,0x00], # 36 6
[0x37,0x00,0xfe, 0x02,0x04,0x04,0x08,0x08,0x08,0x08,0x10,0x10,0x10,0x10,0x10,0x10,0x00],# 37 7
[0x38,0x00,0x38,0x44,0x82,0x82,0x82,0x44,0x38,0x44,0x82,0x82,0x82,0x44,0x38,0x00,0x00], # 38 8
[0x39,0x00,0x38,0x44,0x82,0x82,0x82,0x82,0x46,0x3a, 0x02,0x02,0x82,0x44,0x38,0x00,0x00],# 39 9
[0x3a, 0x00,0x00,0x00,0x00,0x18,0x18,0x00,0x00,0x00,0x00,0x00,0x18,0x18,0x00,0x00,0x00],# 3a :
[0x43,0x00,0x38,0x44,0x42,0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x42,0x44,0x38,0x00,0x00], # 43 C
[0x50,0x00,0xf8,0x84,0x82,0x82,0x82,0x84,0xf8,0x80,0x80,0x80,0x80,0x80,0x80,0x00,0x00], # 50 P
[0x61,0x00,0x00,0x00,0x00,0x00,0x3c,0x42,0x02,0x3e, 0x42,0x82,0x82,0x86,0x7a, 0x00,0x00],# 61 a
[0x68,0x00,0x40,0x40,0x40,0x40,0x5c,0x62,0x42,0x42,0x42,0x42,0x42,0x42,0x42,0x00,0x00], # 68 h
[0x6d, 0x00,0x00,0x00,0x00,0x00,0xec,0x92,0x92,0x92,0x92,0x92,0x92,0x92,0x92,0x00,0x00], # 6d m
[0x73, 0x00,0x00,0x00,0x00,0x00,0x3c,0x42,0x40,0x60,0x18,0x06,0x02,0x42,0x3c,0x00,0x00],# 73 s
[0xe28483,0x0000,0x3072,0x498a, 0x4a06,0x3202,0x0402,0x0400,0x0400,0x0400,0x0400,0x0202,0x0204,0x0198,0x0060,0x0000,0x0000], # 216e ℃
[0xe58886,0x03e0,0x0020,0x0420,0x0410,0x0810,0x0808,0x1004,0x2ff2,0x4111,0x0110,0x0110,0x0210,0x0210,0x0410,0x0810,0x1060], # 4a2c 分
[0xe59091,0x0100,0x0100,0x0200,0x0400,0x3ffe, 0x2002,0x2002,0x27f2,0x2412,0x2412,0x2412,0x27f2,0x2002,0x2002,0x2002,0x2006],　# 387e 向
[0xe59ca7,0x0000,0x1ffe,0x1000,0x1040,0x1040,0x1040,0x1040,0x17fe,0x1040,0x1040,0x1040,0x2040,0x2040,0x5fff, 0x0000,0x0000], # 3035 圧
[0xe5b9b4,0x0400,0x0400,0x0ffe, 0x0840,0x1040,0x2040,0x4ffc, 0x0840,0x0840,0x0840,0x0840,0x7fff, 0x0040,0x0040,0x0040,0x0040],　# 472f 年
[0xe5baa6,0x0040,0x0040,0x1ffe,0x1110,0x1110,0x1ffe,0x1110,0x11f0,0x1000,0x17f8,0x1108,0x1090,0x1060,0x20a0,0x2318,0x5c06], # 4559 度
[0xe697a5,0x0000,0x0ff8,0x0808,0x0808,0x0808,0x0808,0x0808,0x0ff8,0x0808,0x0808,0x0808,0x0808,0x0808,0x0808,0x0ff8,0x0000], # 467c 日
[0xe69982,0x0020,0x0020,0x7dfe,0x4420,0x4420,0x4420,0x47ff,0x7c04,0x4404,0x47ff,0x4504,0x4484,0x7c84,0x0004,0x0004,0x000c],　# 3b7e 時
[0xe69c88,0x0000,0x0ff8,0x0808,0x0808,0x0808,0x0ff8,0x0808,0x0808,0x0808,0x0ff8,0x0808,0x0808,0x1008,0x1008,0x2008,0x4038], # 376e 月
[0xe6b097,0x0800,0x0800,0x0ffe,0x1000,0x17f8,0x2000,0x4000,0x1ff8,0x0048,0x0c48,0x0288,0x0188,0x0245,0x0425,0x1803,0x6001], # 3524 気
[0xe6b8a9,0x1000,0x09fc, 0x0904,0x01fc,0x4104,0x2104,0x21fc, 0x0000,0x0800,0x0bfe,0x1252,0x1252,0x2252,0x2252,0x4fff,0x4000], # 3239 温
[0xe6b9bf,0x1000,0x0bfc, 0x0a04,0x0204,0x03fc,0x4204,0x2204,0x23fc, 0x0800,0x0892,0x1492,0x1294,0x2294,0x2090,0x4fff,0x4000], # 3c3e 湿
[0xe7a792,0x0220,0x0c20,0x7820,0x08a8,0x08a4,0x08a4,0x7ea2,0x0922,0x0c26,0x1a24,0x1968,0x2808,0x2810,0x4820,0x08c0,0x0b00], # 4943 秒
[0xe9809f,0x0020,0x2020,0x17ff,0x1020,0x0020,0x03fe, 0x0222,0x7a22,0x0bfe, 0x0870,0x08a8,0x0924,0x0e23,0x1420,0x2300,0x40ff],　# 422e 速
[0xe9a2a8,0x0000,0x1ff8,0x1028,0x10c8,0x1f08,0x1108,0x17e8,0x1528,0x1528,0x17e8,0x1108,0x1148,0x1125,0x21f5,0x3e13,0x4001], # 4977 風
[0xefbc85,0x0000,0x3802,0x6404,0x4408,0x4410,0x4420,0x4c40,0x3880,0x0138,0x0264,0x0444,0x0844,0x1044,0x204c, 0x4038,0x0000], # 2173 %
)
```

### 電子ペーパー用モジュール
mame_epd266.py
```
from machine import Pin,SPI  
import time
import framebuf
from mame import font16


class Epd266(object):
    # 表示色
    WHITE   = const(0xFF)
    BLACK   = const(0x00)
    # 解像度
    WIDTH        = const(152)
    HEIGHT       = const(296)
    WIDTH_BYTE   = int(WIDTH/8)
    BUFFER_SIZE  = WIDTH_BYTE * HEIGHT
    WF_PARTIAL = bytes([
        0x00,0x40,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x80,0x80,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x40,0x40,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x80,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x0A,0x00,0x00,0x00,0x00,0x00,0x02,0x01,0x00,0x00,
        0x00,0x00,0x00,0x00,0x01,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x22,0x22,0x22,0x22,0x22,0x22,
        0x00,0x00,0x00,0x22,0x17,0x41,0xB0,0x32,0x36,
    ])
    # 使用GPIO
    RST_PIN     = 12
    DC_PIN      = 8
    CS_PIN      = 9
    BUSY_PIN    = 13
    CLK_PIN     = 10
    MOSI_PIN    = 11
    def __init__(self, rotate):
        self.spi = SPI(1)
        self.rate = 4 * 1000 * 1000 # 4MHz
        self.spi.init(baudrate=self.rate, polarity=0, phase=0)
        self.cs     = Pin(self.CS_PIN)
        self.dc     = Pin(self.DC_PIN)
        self.rst    = Pin(self.RST_PIN)
        self.busy   = Pin(self.BUSY_PIN)
        self.clk    = Pin(self.CLK_PIN)
        self.mosi   = Pin(self.MOSI_PIN)
        self.width  = self.WIDTH
        self.height = self.HEIGHT
        self.rotate = rotate
        self.buffer = bytearray(self.BUFFER_SIZE)
        self.framebuf = framebuf.FrameBuffer(self.buffer,self.WIDTH,self.HEIGHT,framebuf.MONO_HLSB)
        self.fontdata = bytes([])
        self.font_size = 16
       
    # 電子ペーパー初期化
    def init(self):
        """ 電子ペーパーを初期化する 
        display()を実行するごとに反転表示を行う
        """
        self.dc.init(self.dc.OUT, value=0)
        self.rst.init(self.rst.OUT, value=0)
        self.cs.init(self.cs.OUT, value=1)
        self.busy.init(self.busy.IN)
        self.reset()
        self.read_busy()
        self.send_cmd(0x12)
        self.read_busy()
        self.send_cmd(0x11)
        self.send_data(0x03)
        self.send_cmd(0x44)
        self.send_data(0x01)
        self.send_data(0x13)
        self.send_cmd(0x45)
        self.send_data(0)
        self.send_data(0)
        self.send_data(0x27)
        self.send_data(0x01)
        self.read_busy()
    # 電子ペーパー初期化（パーシャル）
    def init_partial(self):
        """ 電子ペーパーを初期化する 
        display()を実行した際に反転表示が行われない
        消した文字が残ってしまうことがある場合は、
        init()+clear()を使う
        """
        self.dc.init(self.dc.OUT, value=0)
        self.rst.init(self.rst.OUT, value=0)
        self.cs.init(self.cs.OUT, value=1)
        self.busy.init(self.busy.IN)
        self.reset()
        self.read_busy()
        self.send_cmd(0x12)
        self.read_busy()
        self.set_LUA()
        self.send_cmd(0x37)
        self.send_data(0x00)  
        self.send_data(0x00)  
        self.send_data(0x00)  
        self.send_data(0x00)
        self.send_data(0x00)
        self.send_data(0x40)  
        self.send_data(0x00)  
        self.send_data(0x00)  
        self.send_data(0x00)  
        self.send_data(0x00)
        self.send_cmd(0x11)
        self.send_data(0x03)
        self.send_cmd(0x44)
        self.send_data(0x01)
        self.send_data(0x13)
        self.send_cmd(0x45)
        self.send_data(0)
        self.send_data(0)
        self.send_data(0x27)
        self.send_data(0x01)
        self.send_cmd(0x3C)
        self.send_data(0x80)  
        self.send_cmd(0x22)
        self.send_data(0xcf)
        self.send_cmd(0x20)
        self.read_busy()
    # リセット
    def reset(self):
        self.rst(1)
        time.sleep_ms(200)
        self.rst(0)
        time.sleep_ms(2)
        self.rst(1)
        time.sleep_ms(200)
    # コマンドを送信する
    def send_cmd(self, command):
        self.dc(0)
        self.cs(0)
        self.spi.write(bytes([command]))
        self.cs(1)
    # データを送信する
    def send_data(self, buf):
        self.dc(1)
        self.cs(0)
        self.spi.write(bytes([buf]))
        self.cs(1)
    # busy を確認してアイドリング
    def read_busy(self):
        time.sleep_ms(100)
        while(self.busy == 1): # LOW: idle, HIGH: busy
            time.sleep_ms(100)
        time.sleep_ms(100)
    # LUA の設定
    def set_LUA(self):
        self.send_cmd(0x32)
        for i in self.WF_PARTIAL:
            self.send_data(i)
        self.read_busy()
    # 画面消去
    def clear(self):
        """ 画面一面を白で埋める"""
        self.send_cmd(0x24)
        for i in range(self.BUFFER_SIZE):
            self.send_data(self.WHITE)    #      白で描画
        self.send_cmd(0x20)
        self.read_busy()
        self.display()
    # 待機
    def sleep(self):
        self.send_cmd(0x10)
        self.read_busy()
    # 画面表示
    def display(self):
        """ 表示用配列を利用して画面を表示する"""
        self.send_cmd(0x24)
        for i in range(self.BUFFER_SIZE):
            self.send_data(self.buffer[i])
        self.send_cmd(0x20)
        self.read_busy()
        time.sleep_ms(500)
    # バッファの初期化
    def buffer_clear(self):
        """ 表示用配列を初期化する """
        self.framebuf.fill(1)
    # 画面の回転を設定
    def set_rotate(self,rotate):
        """ 表示画面を回転させる（rotate=、0 , 90 , 180, 270） """
        if rotate == 0 or rotate == 90 or rotate == 180 or rotate == 270:
            self.rotate = rotate
    # 画面の回転に合わせて点を打つ
    def pset(self,px, py, color):
        if self.rotate == 0:
            if (px<self.WIDTH)*(py<self.HEIGHT):
                self.framebuf.pixel(px,py,color)        
        elif self.rotate == 90:
            if (px<self.HEIGHT)*(py<self.WIDTH):
                self.framebuf.pixel(py,self.HEIGHT-px,color)
        elif self.rotate == 180:
            if (px<self.WIDTH)*(py<self.HEIGHT):
                self.framebuf.pixel(self.WIDTH-px,self.HEIGHT-py,color)            
        elif self.rotate == 270:
            if (px<self.HEIGHT)*(py<self.WIDTH):
                self.framebuf.pixel(self.WIDTH-py,px,color)            
        else:
            return
    # 大きな点を打つ
    def mpset(self,px,py,color,ratio):
        for i in range(ratio):
            for j in range(ratio):
                self.pset(px + i, py + j,  color)
    # 表示文字のビットマップ情報を取得
    def get_fontdata(self,code):
        """ フォントのビットマップ情報をリストから取得する """
        fnt = font16.fonta16
        l_num = 0
        h_num = len(fnt)-1


        while(l_num <= h_num): # 二分探索を使って、
            m_num = int((h_num+l_num)/2)
            if(int(code) == fnt[m_num][0]):
                return fnt[m_num]
            elif(int(code) < fnt[m_num][0]):
                h_num = m_num-1
            else:
                l_num = m_num+1
        return fnt[0]   # フォントが見つからなかった場合
    # 文字を書く
    def print( self,dx, dy, buf,ratio):
        """ 開始位置(dx,dy）からbufの文字列を書く、ratioは、拡大率
        画面に表示する場合には、display()を実行する """
        # 縦横の向きに合わせて幅サイズを調整
        if self.rotate == 0 or self.rotate == 180:
            wx=self.WIDTH
        else:
            wx=self.HEIGHT
        for ch in buf:
            self.fontdata=self.get_fontdata("0x"+ch.encode('utf-8').hex())  # フォントデータを取得
            if ord(ch) < 0x7F: # ASCIIコードの場合(8x16)
                for j in range(self.font_size):
                    if (dx > wx-int(self.font_size/2*ratio)): # 改行の判定
                        dx=0
                        dy+=self.font_size*ratio
                    for i in range(self.font_size/2):
                        if(int(self.fontdata[j+1])) & (0x80 >> int(i%(self.font_size/2))):
                            self.mpset(dx+i*ratio,dy+j*ratio,self.BLACK,ratio)
                        else:
                            self.mpset(dx+i*ratio,dy+j*ratio,self.WHITE,ratio)
                dx+=int(self.font_size/2*ratio)
            else:   # 日本語の場合(16x16)
                for j in range(self.font_size):
                    if (dx > wx-int(self.font_size*ratio)): # 改行の判定
                        dx=0
                        dy+=self.font_size*ratio
                    for i in range(self.font_size):
                        if(int(self.fontdata[j+1])) & (0x8000 >> int(i%(self.font_size))):
                            self.mpset(dx+i*ratio,dy+j*ratio,self.BLACK,ratio)
                        else:
                            self.mpset(dx+i*ratio,dy+j*ratio,self.WHITE,ratio)
                dx+=int(self.font_size*ratio)
    # 線をひく
    def line(self,sx,sy,ex,ey):
        """開始位置(sx,sy)から、終了位置(ex,ey)まで直線を描く 画面に表示する場合には、display()を実行する"""
        if self.rotate == 0:
            self.framebuf.line(sx,sy,ex,ey,self.BLACK)
        elif self.rotate == 90:
            self.framebuf.line(sy,self.HEIGHT-sx,ey,self.HEIGHT-ex,self.BLACK)
        elif self.rotate == 180:
            self.framebuf.line(self.WIDTH-sx,self.HEIGHT-sy,self.WIDTH-ex,self.HEIGHT-ey,self.BLACK)
        elif self.rotate == 270:
            self.framebuf.line(self.WIDTH-sy,sx,self.WIDTH-ey,ex,self.BLACK)
        else:
            return
```



### プログラム例
```
# main.py
#
# mame_epd266 library
# Copyright (c) Daicyamame All rights reserved.
# https://plaza.rakuten.co.jp/washiinuru/diary/202308240000/
#
from mame import okdhryk_epd266

epd = okdhryk_epd266.Epd266(rotate=0)
epd.init()
epd.clear()

epd.init_partial()
epd.buffer_clear()
epd.print(0,0," %,-./0123456789",1)
epd.print(0,20,":CPahms",1)
epd.print(0,40,"℃分向圧年度日時",1)
epd.print(0,60,"月気温湿秒速風％",1)
epd.display()
```

```
from okdhryk import okdhryk_epd266

epd = okdhryk_epd266.Epd266(rotate=0)
epd.init()
epd.clear()

epd.init_partial()
epd.buffer_clear()
epd.print(8,0,"2023年",1)
epd.print(16,20,"11月11日",2)
epd.line(0,60,151,60)
epd.print(15,65,"23:34",3)
epd.display()

epd.set_rotate(90)
epd.buffer_clear()
epd.print(8,0,"2023年11月11日",2)
epd.line(0,40,292,40)
epd.print(40,60,"23:45",5)
epd.display()
```

