# Neopixel フルカラーLED




## Neopixel 
Neopixel は、Adafruit（エーダフルート）社が販売する、フルカラーLEDです。 

LED１個ずつにICチップ(WS2812B(Worldsemi製))が入っており、光る色や明るさを１個ずつ変えることができます。

単体、テープタイプ、円形など、様々なタイプのNeopixel商品があります。


## Neopixel RGB リング24　WS2812B
<img src="../images/61uULqhrg9L._SL1500_.jpg" width=512>

- 電圧: 5V DC
- LED: 24ビット
- 内径: 7cm/2.8in
- 外径: 8.5cm/3.3in
- 重量: 8g (約)



## NeoPixel RGB スルーホール(5mm)

## Neopixel RGB ストリング　WS2812B

## Pythonライブラリ
Raspberry Pi Pico用のmicroPythonライブラリです。
ws2812b and sk6812 に対応しています。

[pi_pico_neopixel](https://github.com/blaz-r/pi_pico_neopixel/tree/main)

[WiKi](https://github.com/blaz-r/pi_pico_neopixel/wiki)
[Documentation](https://github.com/blaz-r/pi_pico_neopixel/wiki/Library-methods-documentation)

